package com.intentsolutions.aventuretrap;

import com.intentsolutions.aventuretrap.model.Post;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

public class PostTest {

    @Test
    public void postEquals() throws Exception {
        Post p1 = new Post("1", "image2", "asd3", "aaa4", "2017-05-10 12:00:00", 6, "7", 8, "1");
        Post p2 = new Post("1", "image2", "asd3", "aaa4", "2017-05-10 12:00:00", 6, "7", 8, "1");
        assertTrue("p1 and p2 should be equals as they has code", p1.equals(p2) && p1.hashCode() == p2.hashCode());
        /*List<Post> list = new LinkedList<>();
        list.add(p1);
        list.add(p2);
        Collections.sort(list);*/
    }
}
