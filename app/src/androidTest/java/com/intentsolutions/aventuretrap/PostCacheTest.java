package com.intentsolutions.aventuretrap;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.intentsolutions.aventuretrap.model.Category;
import com.intentsolutions.aventuretrap.cache.CategoryCache;
import com.intentsolutions.aventuretrap.model.Post;
import com.intentsolutions.aventuretrap.cache.PostCache;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class PostCacheTest {
    private Context appContext;
    private PostCache db;

    @Before
    public void setUpData() {
        System.out.println("asdasdsada");
        appContext = InstrumentationRegistry.getTargetContext();
        db = new PostCache(appContext);
    }

    @Test
    public void dbTest() throws Exception {
        System.out.println("asdasdsada2");
        //assertEquals("com.intentsolutions.aventuretrap", appContext.getPackageName());

        CategoryCache cc = new CategoryCache(appContext);
        cc.cleanCategory();
        Category c1 = new Category();
        c1.setPostIdTop(1);
        c1.setCategoryId(1);
        c1.setCategoryName("cat_1");
        c1.setCategoryDesk("desc 1");
        cc.add(c1);
        Category c2 = new Category();
        c2.setPostIdTop(22);
        c2.setCategoryId(2);
        c2.setCategoryName("cat_2");
        c2.setCategoryDesk("desc 2");
        cc.add(c2);

        db.cleanPost();

        Post p1 = new Post("1", "image2", "asd3", "aaa4", "2017-05-10", 1, "7", 8, "1");
        Post p2 = new Post("9", "image10", "asd11", "aaa12", "2017-03-10", 2, "15", 16, "0");
        db.add(p1);
        db.add(p2);

        Log.d("dev", "post count: " + db.getPostCount(1));

        List<Post> posts =  db.getPostsByCat(1);

        Log.d("dev", "posts 1: ");
        for (Post p: posts) {
            Log.d("dev", p.getPostId() + "; " + p.getPostName());
        }
        posts =  db.getPostsByCat(2);

        Log.d("dev", "posts 2: ");
        for (Post p: posts) {
            Log.d("dev", p.getPostId() + "; " + p.getPostName());
        }
        db.cleanPost(1);

        posts =  db.getPostsByCat(1);

        Log.d("dev", "posts 1: ");
        for (Post p: posts) {
            Log.d("dev", p.getPostId() + "; " + p.getPostName());
        }
        db.add(p1);
        //assertTrue("Post 0 after fetch is not equal to first copy", posts.get(0).equals(p1));
        //assertTrue("Post 1 after fetch is not equal to second copy", posts.get(1).equals(p2));
    }

    @Test
    public void postActions() throws Exception {
        //assertEquals("com.intentsolutions.aventuretrap", appContext.getPackageName());

        db.cleanPost();
        assertEquals("Post count after cleanPost() call should be zero", 0, db.getPostCount(6));
        Post p1 = new Post("1", "image2", "asd3", "aaa4", "2017-05-10", 6, "7", 8, "1");
        Post p2 = new Post("9", "image10", "asd11", "aaa12", "2017-03-10", 6, "15", 16, "0");
        db.add(p1);
        db.add(p2);
        assertEquals("Post count after two postAdd() calls should be 2", 2, db.getPostCount(6));

        List<Post> posts = db.getAllPosts();
        assertTrue("Post 0 after fetch is not equal to first copy", posts.get(0).equals(p1));
        assertTrue("Post 1 after fetch is not equal to second copy", posts.get(1).equals(p2));
    }
}