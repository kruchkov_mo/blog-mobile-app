package com.intentsolutions.aventuretrap.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.intentsolutions.aventuretrap.model.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 2/5/2018.
 */

public class PostCache extends DbCache<Post> {

    public PostCache(Context context) {
        super(context);
    }

    public boolean cleanPost() {
        return super.cleanTable(TABLE_POST);
    }

    public boolean cleanPost(Integer categoryId) {
        if (categoryId == null) {
            return false;
        }
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL(String.format("DELETE FROM %s WHERE category_id = %d", TABLE_POST, categoryId));
        } catch (SQLException e) {
            Log.d("Exception message", e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public List<Post> getItems(String SQL_QUERY) {
        List<Post> postList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SQL_QUERY, null);

        if (cursor.moveToFirst()) {
            do {
                Post post = new Post();
                post.setPostId(cursor.getString(0));
                post.setCategoryId(cursor.getInt(1));
                post.setProductToCategoryId(cursor.getString(2));
                post.setPostName(cursor.getString(3));
                post.setPostImage(cursor.getString(4));
                post.setPostDesk(cursor.getString(5));
                post.setPostDate(cursor.getString(6));
                post.setPostUserId(Integer.parseInt(cursor.getString(7)));
                post.setPostStatus(cursor.getString(8));
                postList.add(post);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return postList;
    }

    @Override
    public void add(Post item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_POST_ID, item.getPostId());
        values.put(KEY_POST_USER_ID, item.getPostUserId());
        values.put(KEY_CAT_ID, item.getCategoryId());
        values.put(KEY_PROD_TO_CAT_ID, item.getProductToCategoryId());
        values.put(KEY_POST_NAME, item.getPostName());
        values.put(KEY_POST_DESK, item.getPostDesk());
        values.put(KEY_POST_DATE, item.getPostDate());
        values.put(KEY_POST_IMAGE, item.getPostImage());
        values.put(KEY_POST_STATUS, item.getPostStatus());

        db.insert(TABLE_POST, null, values);
        db.close();
    }

    public List<Post> getPostsByCat(final int categoryId) {
        List<Post> postList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(String.format("SELECT * FROM %s WHERE category_id = %d", TABLE_POST, categoryId), null);

        if (cursor.moveToFirst()) {
            do {
                Post post = new Post();
                post.setPostId(cursor.getString(0));
                post.setCategoryId(cursor.getInt(1));
                post.setProductToCategoryId(cursor.getString(2));
                post.setPostName(cursor.getString(3));
                post.setPostImage(cursor.getString(4));
                post.setPostDesk(cursor.getString(5));
                post.setPostDate(cursor.getString(6));
                post.setPostUserId(Integer.parseInt(cursor.getString(7)));
                post.setPostStatus(cursor.getString(8));
                postList.add(post);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return postList;
    }

    public List<Post> getAllPosts() {
        return getItems("SELECT * FROM " + TABLE_POST);
    }

    public long getPostCount(final int categoryId) {
        String countQuery = String.format("SELECT * FROM %s WHERE category_id = %d", TABLE_POST, categoryId);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
 /*

        // Banner
    public void addBanner(Banner banner) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IMAGE, banner.getImg());
        values.put(KEY_URL, banner.getUrl());
        values.put(KEY_VISIBILITY, banner.getVisible() ? "1" : "0");

        db.insert(TABLE_BANNER, null, values);
        db.close();
    }

    public boolean cleanBanner() {
        return this.cleanTable(TABLE_BANNER);
    }

    public List<Banner> getAllBanners() {
        List<Banner> bannerList = new ArrayList<Banner>();
        String selectQuery = "SELECT * FROM " + TABLE_BANNER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Banner banner = new Banner();
                banner.setImg(cursor.getString(1));
                banner.setUrl(cursor.getString(2));
                banner.setVisible(!cursor.getString(3).equals("0"));
                bannerList.add(banner);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return bannerList;
    }

    public Banner getBanner(int bannerId) {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns =  new String[] {
            KEY_BANNER_ID,
            KEY_IMAGE,
            KEY_URL,
            KEY_VISIBILITY
        };
        Cursor cursor = db.query(TABLE_BANNER, columns, KEY_BANNER_ID + "=?",
                new String[] { String.valueOf(bannerId) }, null, null, null, null);
        Banner banner = null;
        if (cursor != null) {
            cursor.moveToFirst();
            Boolean isVisible = !cursor.getString(3).equals("0");
            banner = new Banner(cursor.getString(1), cursor.getString(2), isVisible);
            cursor.close();
        }
        return banner;
    }

    public int getBannersCount() {
        String countQuery = "SELECT * FROM " + TABLE_BANNER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }
    */
}