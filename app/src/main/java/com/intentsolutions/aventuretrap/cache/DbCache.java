package com.intentsolutions.aventuretrap.cache;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.List;

/**
 * Created by max on 3/6/2018.
 */

public abstract class DbCache<E> extends SQLiteOpenHelper {
    protected final static int DATABASE_VERSION = 3;
    protected final static String DATABASE_NAME = "aventuretrap_cache";

    protected static final String TABLE_POST = "post";

    protected static final String KEY_POST_ID = "post_id";
    protected static final String KEY_CAT_ID = "category_id";
    protected static final String KEY_PROD_TO_CAT_ID = "product_to_category_id";
    protected static final String KEY_POST_NAME = "name";
    protected static final String KEY_POST_IMAGE = "image";
    protected static final String KEY_POST_DESK = "description";
    protected static final String KEY_POST_DATE = "date";
    protected static final String KEY_POST_USER_ID = "user_id";
    protected static final String KEY_POST_STATUS = "status";

    protected static final String TABLE_CATEGORY = "category";

    protected static final String KEY_CATEGORY_ID = "category_id";
    protected static final String KEY_CATEGORY_NAME = "name";
    protected static final String KEY_CATEGORY_DESC = "desc";
    protected static final String KEY_CATEGORY_POST_ID_TOP = "post_id_top";

    public DbCache(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_POST_TABLE = "CREATE TABLE " + TABLE_POST + " (" +
                KEY_POST_ID + " INTEGER PRIMARY KEY, " +
                KEY_CAT_ID + " INTEGER," +
                KEY_PROD_TO_CAT_ID + " TEXT," +
                KEY_POST_NAME + " TEXT," +
                KEY_POST_IMAGE + " TEXT," +
                KEY_POST_DESK + " TEXT," +
                KEY_POST_DATE + " TEXT," +
                KEY_POST_USER_ID + " INTEGER," +
                KEY_POST_STATUS + " TEXT)";
        db.execSQL(CREATE_POST_TABLE);

        String CREATE_CATEGORY_TABLE = "CREATE TABLE " + TABLE_CATEGORY + "("
                + KEY_CATEGORY_ID + " INTEGER PRIMARY KEY,"
                + KEY_CATEGORY_NAME + " TEXT,"
                + KEY_CATEGORY_DESC + " TEXT,"
                + KEY_CATEGORY_POST_ID_TOP + " INTEGER DEFAULT 0)";

        db.execSQL(CREATE_CATEGORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POST);
        onCreate(db);
    }

    public boolean save(List<E> items) {
        if (items == null) {
            return false;
        }
        for (E c : items) {
            this.add(c);
        }
        return true;
    }

    protected boolean cleanTable(final String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + tableName);
        } catch (SQLException e) {
            Log.d("Exception message", e.getMessage());
            return false;
        }
        return true;
    }

    public abstract List<E> getItems(final String SQL_QUERY);

    public abstract void add(E item);
}