package com.intentsolutions.aventuretrap.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;

import com.intentsolutions.aventuretrap.BuildConfig;
import com.jakewharton.disklrucache.DiskLruCache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by max on 3/27/2018.
 */

public class DiskDrawableCache {

    private DiskLruCache mDiskCache;
    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
    private int mCompressQuality = 70;
    private static final int APP_VERSION = 1;
    private Context context = null;
    private static final int VALUE_COUNT = 1;
    private static final String TAG = "DiskLruImageCache";

    public DiskDrawableCache(Context context, String uniqueName, int diskCacheSize,
                          Bitmap.CompressFormat compressFormat, int quality) {
        try {
            final File diskCacheDir = getDiskCacheDir(context, uniqueName);
            mDiskCache = DiskLruCache.open(diskCacheDir, APP_VERSION, VALUE_COUNT, diskCacheSize);
            mCompressFormat = compressFormat;
            mCompressQuality = quality;
            this.context = context;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean writeBitmapToFile(Bitmap bitmap, DiskLruCache.Editor editor) throws IOException {
        try (OutputStream out = new BufferedOutputStream(editor.newOutputStream(0), Utils.IO_BUFFER_SIZE)) {
            return bitmap.compress(mCompressFormat, mCompressQuality, out);
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "writeBitmapToFile() fails: " + e.getMessage());
            }
        }
        return false;
    }

    private File getDiskCacheDir(Context context, String uniqueName) throws IOException {
        final String cachePath =
                Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                        !Utils.isExternalStorageRemovable() ?
                        Utils.getExternalCacheDir(context).getPath() :
                        context.getCacheDir().getPath();

        File cacheFolder = new File(cachePath + File.separator + uniqueName);
        if (!cacheFolder.exists()) {
            cacheFolder.mkdirs();
        }
        return new File(cachePath + File.separator + uniqueName);
    }

    private static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public void put(String key, Drawable data) {
        DiskLruCache.Editor editor = null;
        try {
            editor = mDiskCache.edit(key);
            if (editor == null) {
                return;
            }

            if(writeBitmapToFile(drawableToBitmap(data), editor)) {
                mDiskCache.flush();
                editor.commit();
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "image put on disk cache " + key);
                }
            } else {
                editor.abort();
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "ERROR on: image put on disk cache " + key);
                }
            }
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "ERROR on: image put on disk cache " + key);
            }
            try {
                if (editor != null) {
                    editor.abort();
                }
            } catch (IOException ignored) {
            }
        }
    }

    public Bitmap getAsBitmap(String key) {
        Bitmap bitmap = null;
        try (DiskLruCache.Snapshot snapshot = mDiskCache.get(key)) {
            if (snapshot == null) {
                return null;
            }
            try (InputStream in = snapshot.getInputStream(0)) {
                if (in != null) {
                    final BufferedInputStream buffIn =
                            new BufferedInputStream(in, Utils.IO_BUFFER_SIZE);
                    bitmap = BitmapFactory.decodeStream(buffIn);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, bitmap == null ? "" : "image read from disk " + key);
        }
        return bitmap;
    }

    public Drawable getAsDrawable(String key) {
        Bitmap bitmap = getAsBitmap(key);
        if (bitmap != null) {
            return new BitmapDrawable(context.getResources(), bitmap);
        }
        return null;
    }

    public boolean containsKey(String key) {
        boolean contained = false;
        try (DiskLruCache.Snapshot snapshot = mDiskCache.get(key)) {
            contained = snapshot != null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contained;
    }

    public void clearCache() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "disk cache CLEARED");
        }
        try {
            mDiskCache.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getCacheFolder() {
        return mDiskCache.getDirectory();
    }
}