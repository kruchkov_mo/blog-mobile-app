package com.intentsolutions.aventuretrap.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.intentsolutions.aventuretrap.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 2/5/2018.
 */

public class CategoryCache extends DbCache<Category> {

    public CategoryCache(Context context) {
        super(context);
    }

    @Override
    public List<Category> getItems(final String SQL_QUERY) {
        List<Category> categories = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SQL_QUERY, null);

        if (cursor.moveToFirst()) {
            do {
                Category category = new Category();
                category.setCategoryId(cursor.getInt(0));
                category.setCategoryName(cursor.getString(1));
                category.setCategoryDesk(cursor.getString(2));
                category.setPostIdTop(cursor.getInt(3));

                categories.add(category);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return categories;
    }

    public List<Category> getAllCategories() {
        return  getItems("SELECT * FROM " + TABLE_CATEGORY);
    }

    @Override
    public void add(Category item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_CATEGORY_ID, item.getCategoryId());
        values.put(KEY_CATEGORY_DESC, item.getCategoryDesk());
        values.put(KEY_CATEGORY_NAME, item.getCategoryName());
        values.put(KEY_CATEGORY_POST_ID_TOP, item.getPostIdTop());

        db.insert(TABLE_CATEGORY, null, values);
        db.close();
    }

    public boolean cleanCategory() {
        return super.cleanTable(TABLE_CATEGORY);
    }
}