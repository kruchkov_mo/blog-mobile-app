package com.intentsolutions.aventuretrap.model;

/**
 * Created by max on 5/2/2018.
 */

public class CommentItem {
    public CommentItem(String userName, String dateAdded, String commentText) {
        this.userName = userName;
        this.dateAdded = dateAdded;
        this.commentText = commentText;
    }

    public String userName;
    public String dateAdded;
    public String commentText;
}
