package com.intentsolutions.aventuretrap.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

@IgnoreExtraProperties

public class BlogPost {
    public List<String> commentsIds;
    public List<String> likeUsersids;
    //public Integer like = 0;
    public String postId;

    public BlogPost() {}

    public BlogPost(String postId, List<String> commentsIds, List<String> likes) {
        this.commentsIds = commentsIds;
        this.likeUsersids = likes;
        this.postId = postId;
    }
}
