package com.intentsolutions.aventuretrap.model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.intentsolutions.aventuretrap.R;

import java.util.List;

/**
 * Created by max on 4/27/2018.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    private List<CommentItem> blogComments;

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        public TextView text, date, author;

        public CommentViewHolder(View view) {
            super(view);
            text = (TextView) view.findViewById(R.id.comment_text);
            date = (TextView) view.findViewById(R.id.comment_date);
            author = (TextView) view.findViewById(R.id.comment_author);
        }
    }

    public CommentAdapter(List<CommentItem> list) {
        this.blogComments = list;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false);

        return new CommentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        CommentItem comment = blogComments.get(position);
        holder.text.setText(comment.commentText);
        holder.date.setText(comment.dateAdded);
        holder.author.setText(comment.userName);
    }

    @Override
    public int getItemCount() {
        return blogComments.size();
    }
}