package com.intentsolutions.aventuretrap.model;
/**
 * Created by Artem Koshkin on 13.11.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Banner {

    @SerializedName("banner_status")
    @Expose
    private Boolean visible;
    @SerializedName("banner_image")
    @Expose
    private String img;
    @SerializedName("banner_url")
    @Expose
    private String url;

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
