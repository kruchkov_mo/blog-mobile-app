package com.intentsolutions.aventuretrap.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Post implements Parcelable, Comparable<Post> {

    @SerializedName("product_to_category_id")
    @Expose
    private String productToCategoryId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("post_name")
    @Expose
    private String postName;
    @SerializedName("post_image")
    @Expose
    private String postImage;
    @SerializedName("post_desk")
    @Expose
    private String postDesk;
    @SerializedName("post_date")
    @Expose
    private String postDate;

    @Override
    public int compareTo(@NonNull Post other) {
        String d1 = this.getPostDate();
        String d2 = other.getPostDate();
        final DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        try {
            Date date1 = format.parse(d1);
            Date date2 = format.parse(d2);
            if (date1.before(date2)) {
                return 1;
            } else if (date1.after(date2)) {
                return -1;
            }
            //return date1.compareTo(date2);
        } catch (ParseException e) {
            Log.d("class Post", e.getMessage());
        }

        return 0;
    }

    @SerializedName("post_user_id")
    @Expose
    private int postUserId;
    @SerializedName("post_status")
    @Expose
    private String postStatus;

    public String getProductToCategoryId() {
        return productToCategoryId;
    }

    public void setProductToCategoryId(String productToCategoryId) {
        this.productToCategoryId = productToCategoryId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getPostDesk() {
//        if(postDesk.startsWith("<p>") && postDesk.endsWith("</p>")) {
//            String answer = postDesk.substring(3,postDesk.length()-4);
//            return answer;
//        }
        return postDesk;
    }

    public void setPostDesk(String postDesk) {
        this.postDesk = postDesk;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public int getPostUserId() {
        return postUserId;
    }

    public void setPostUserId(int postUserId) {
        this.postUserId = postUserId;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.productToCategoryId);
        dest.writeInt(this.categoryId);
        dest.writeString(this.postId);
        dest.writeString(this.postName);
        dest.writeString(this.postImage);
        dest.writeString(this.postDesk);
        dest.writeString(this.postDate);
        dest.writeInt(this.postUserId);
        dest.writeString(this.postStatus);
    }

    public Post() {
    }

    public Post(String postId, String image, String name, String desc, String date, Integer catId, String prodToCatId, Integer userId, String status) {
        this.productToCategoryId = prodToCatId;
        this.categoryId = catId;
        this.postId = postId;
        this.postName = name;
        this.postImage = image;
        this.postDesk = desc;
        this.postDate = date;
        this.postUserId = userId;
        this.postStatus = status;
    }

    protected Post(Parcel in) {
        this.productToCategoryId = in.readString();
        this.categoryId = in.readInt();
        this.postId = in.readString();
        this.postName = in.readString();
        this.postImage = in.readString();
        this.postDesk = in.readString();
        this.postDate = in.readString();
        this.postUserId = in.readInt();
        this.postStatus = in.readString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Post)) {
            return false;
        }
        final Post other = (Post) obj;
        if (this.categoryId.equals(other.categoryId) &&
            this.postId.equals(other.postId) &&
            this.postDate.equals(other.postDate) &&
            this.productToCategoryId.equals(other.productToCategoryId) &&
            this.postUserId == other.postUserId &&
            this.postImage.equals(other.postImage) &&
            this.postDesk.equals(other.postDesk) &&
            this.postName.equals(other.postName) &&
            this.postStatus.equals(other.postStatus)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int postId = 0;
        try {
            postId = Integer.parseInt(this.postId);
        } catch (NumberFormatException e) {
            Log.d("Post", e.getMessage());
        }
        int prime = 31;
        return prime + postId;
    }
    public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };
}