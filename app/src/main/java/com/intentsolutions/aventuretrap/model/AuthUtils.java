package com.intentsolutions.aventuretrap.model;

import android.net.Uri;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


/**
 * Created by max on 4/26/2018.
 */

public class AuthUtils {
    public static void addNewUser(DatabaseReference ref, FirebaseUser user) {
        if (ref == null || user == null) {
            return;
        }

        Uri uri = user.getPhotoUrl();
        String uid = user.getUid();
        BlogUser u = new BlogUser(uid, user.getDisplayName(), user.getEmail(), uri == null ? "" : uri.toString());
        ref.child(uid).setValue(u);
    }

    public static void addNewPost(DatabaseReference ref, BlogPost post) {
        if (ref == null || post == null) {
            return;
        }

        ref.child(post.postId).setValue(post);
    }

    public static void addNewComment(DatabaseReference ref, BlogComment comment) {
        if (ref == null || comment == null) {
            return;
        }

        ref.child(comment.commentId).setValue(comment);
    }

    public static void likePost(DatabaseReference ref, String uid) {
       if (ref == null) {
            return;
        }
        ref.setValue(uid);
    }
}
