package com.intentsolutions.aventuretrap.model;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.intentsolutions.aventuretrap.R;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.BaseViewHolder> {
    private Context context;
    private List<Post> data;
    private Typeface font, fontBold;
    private final ClickListener clickListener;

    enum ITEM_TYPE {CONTENT_TYPE, AD_TYPE}

    public PostAdapter(Context context, List<Post> data, ClickListener clickListener) {
        this.context = context;
        this.data = data;
        fontBold = Typeface.createFromAsset(context.getAssets(), "Scada-Bold.ttf");
        font = Typeface.createFromAsset(context.getAssets(), "Scada-Regular.ttf");
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE.AD_TYPE.ordinal()) {
            // adMob init
            MobileAds.initialize(context, context.getString(R.string.ad_app_id));
            AdView adView = new AdView(parent.getContext());
            adView.setAdSize(AdSize.LARGE_BANNER);
            adView.setAdUnitId(context.getString(R.string.ad_unit_id));
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
            return new AdViewHolder(adView);
        }
        Context c = parent.getContext();
        Log.d("inflate", "context: " + c);
        LayoutInflater layoutInflater = LayoutInflater.from(c);
        Log.d("inflate", "inflater: " + layoutInflater);
        View view = layoutInflater.inflate(R.layout.item_post, parent, false);
        return new ViewHolder(view, clickListener);
    }

    public Post getItem(int position) {
        return data.get(position);
    }

    private static String stripHtml(String html) {
        return html.replaceAll("\\<.*?>","");
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.customBind(holder, position);
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) == null) {
            return ITEM_TYPE.AD_TYPE.ordinal();
        }
        return ITEM_TYPE.CONTENT_TYPE.ordinal();
    }

    public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(View itemView) {
            super(itemView);
        }
        public abstract void customBind(BaseViewHolder holder, int position);
    }
    public class AdViewHolder extends BaseViewHolder {
        public AdViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void customBind(BaseViewHolder holder, int position) {
        }
    }

    public class ViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.item_img)
        ImageView img;
        @BindView(R.id.item_title)
        TextView title;
        @BindView(R.id.item_des)
        TextView des;
        @BindView(R.id.detail)
        TextView detail;

        private WeakReference<ClickListener> listenerRef;

        public ViewHolder(View itemView, ClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (detail == null) {
                Log.d("Null", "Details button object ois null");
            } else {
                detail.setOnClickListener(this);
            }
            listenerRef = new WeakReference<>(listener);
        }

        @Override
        public void customBind(BaseViewHolder viewHolder, int position) {
            Post post = data.get(position);
            if (viewHolder instanceof ViewHolder && post != null) {
                ViewHolder holder = (ViewHolder) viewHolder;
                if (fontBold != null) holder.title.setTypeface(fontBold);
                holder.des.setTypeface(font);
                holder.title.setText(post.getPostName());
                holder.des.setText(stripHtml(post.getPostDesk()));
                if (post.getPostImage() != null) {
                    Glide.with(context)
                            .load(post.getPostImage())
                            .error(R.drawable.no_photo)
                            .into(holder.img);
                } else {
                    holder.img.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onClick(View view) {
            ClickListener item = listenerRef.get();
            if (item != null) {
                item.onItemClick(getAdapterPosition(), view);
            }
        }
    }
}