package com.intentsolutions.aventuretrap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by max on 2/28/2018.
 */

public class Category {
    @SerializedName("category_name")
    @Expose
    private String categoryName = null;

    @SerializedName("category_id")
    @Expose
    private Integer categoryId = null;

    @SerializedName("category_desk")
    @Expose
    private String categoryDesk = null;

    @SerializedName("post_id_top")
    @Expose
    private Integer postIdTop = null;

    public String getCategoryName() {
        return categoryName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getCategoryDesk() {
        return categoryDesk;
    }

    public Integer getPostIdTop() {
        return postIdTop;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public void setCategoryDesk(String categoryDesk) {
        this.categoryDesk = categoryDesk;
    }

    public void setPostIdTop(Integer postIdTop) {
        this.postIdTop = postIdTop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (categoryName != null ? !categoryName.equals(category.categoryName) : category.categoryName != null)
            return false;
        if (categoryId != null ? !categoryId.equals(category.categoryId) : category.categoryId != null)
            return false;
        if (categoryDesk != null ? !categoryDesk.equals(category.categoryDesk) : category.categoryDesk != null)
            return false;
        return postIdTop != null ? postIdTop.equals(category.postIdTop) : category.postIdTop == null;
    }

    @Override
    public int hashCode() {
        int result = categoryName != null ? categoryName.hashCode() : 0;
        result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
        result = 31 * result + (categoryDesk != null ? categoryDesk.hashCode() : 0);
        result = 31 * result + (postIdTop != null ? postIdTop.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" + categoryId + '}';
    }
}