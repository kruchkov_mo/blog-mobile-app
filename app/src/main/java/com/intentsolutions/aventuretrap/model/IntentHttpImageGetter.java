package com.intentsolutions.aventuretrap.model;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

import com.intentsolutions.aventuretrap.R;
import com.intentsolutions.aventuretrap.cache.DiskDrawableCache;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URL;

/**
 * Created by max on 3/5/2018.
 */

public class IntentHttpImageGetter implements Html.ImageGetter {
    TextView container;
    private Activity activity;
    private boolean compressImage = false;
    private int qualityImage = 50;
    private static String TAG = "ImageGetter";
    private final static int DEFAULT_CACHE_SIZE = 30 * 1024 * 1024;

    private DiskDrawableCache cache = null;

    public IntentHttpImageGetter(TextView textView, Activity activity) {
        this.container = textView;
        this.activity = activity;
        int diskCacheSize = 0;
        try {
            String strDiskCacheSize = activity.getString(R.string.disk_cache_size);
            diskCacheSize = Integer.parseInt(strDiskCacheSize);
        } catch (NumberFormatException e) {
            Log.d(TAG, "Fetching disk cache size from settings fails: " + e.getMessage());
            diskCacheSize = DEFAULT_CACHE_SIZE;
        }

        this.cache = new DiskDrawableCache(activity, "image_cache", diskCacheSize, Bitmap.CompressFormat.JPEG, 80);
    }

    public void enableCompressImage(boolean enable) {
        enableCompressImage(enable, 50);
    }

    public void enableCompressImage(boolean enable, int quality) {
        compressImage = enable;
        qualityImage = quality;
    }

    public Drawable getDrawable(String source) {
        IntentHttpImageGetter.UrlDrawable urlDrawable = new IntentHttpImageGetter.UrlDrawable();
        // get the actual source
        IntentHttpImageGetter.ImageGetterAsyncTask asyncTask =
                new IntentHttpImageGetter.ImageGetterAsyncTask(urlDrawable,
                        this,
                        container,
                        compressImage,
                        qualityImage,
                        activity);
        asyncTask.execute(source);
        // return reference to URLDrawable which will asynchronously load the image specified in the src tag
        return urlDrawable;
    }

    /**
     * Static inner {@link AsyncTask} that keeps a {@link WeakReference} to the {@link IntentHttpImageGetter.UrlDrawable}
     * and {@link IntentHttpImageGetter}.
     * <p/>
     * This way, if the AsyncTask has a longer life span than the UrlDrawable,
     * we won't leak the UrlDrawable or the HtmlRemoteImageGetter.
     */
    private static class ImageGetterAsyncTask extends AsyncTask<String, Void, Drawable> {
        private final IntentHttpImageGetter.UrlDrawable drawableReference;
        private final IntentHttpImageGetter imageGetterReference;
        private final Resources resources;
        private String source;
        private float scale;
        private boolean compressImage = false;
        private final WeakReference<Activity> activity;
        private int qualityImage = 50;

        public ImageGetterAsyncTask(IntentHttpImageGetter.UrlDrawable d,
                                    IntentHttpImageGetter imageGetter, View container,
                                    boolean compressImage,
                                    int qualityImage, Activity activity) {
            this.drawableReference = d;
            this.imageGetterReference = imageGetter;
            this.resources = container.getResources();
            this.compressImage = compressImage;
            this.qualityImage = qualityImage;
            this.activity = new WeakReference<>(activity);
        }

        @Override
        protected Drawable doInBackground(String... params) {
            source = params[0];
            if (resources != null) {
                if (compressImage) {
                    return fetchCompressedDrawable(resources, source);
                } else {
                    return fetchDrawable(resources, source);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Drawable result) {
            if (result == null) {
                Log.w(TAG, "Drawable result is null! (source: " + source + ")");
                return;
            }
            if (drawableReference == null) {
                return;
            }
            // set the correct bound according to the result from HTTP call
            drawableReference.setBounds(0, 0, (int) (result.getIntrinsicWidth() * scale), (int) (result.getIntrinsicHeight() * scale));

            // change the reference of the current drawable to the result from the HTTP call
            drawableReference.drawable = result;
            final IntentHttpImageGetter imageGetter = imageGetterReference;
            if (imageGetter == null) {
                return;
            }
            // redraw the image by invalidating the container
            imageGetter.container.invalidate();
            // re-set text to fix images overlapping text
            imageGetter.container.setText(imageGetter.container.getText());
        }

        private static String extractKey(String url) {
            String fileName = url.substring(url.lastIndexOf('/') + 1);
            String s = fileName.replaceAll("\\.|\\s|,|\\)|\\(", "_");
            return s.toLowerCase().replaceAll("[^a-z0-9_]+", "x"); //s.replaceAll("[^a-z0-9_]+", "x").toLowerCase();
        }

        /**
         * Get the Drawable from URL
         */
        public Drawable fetchDrawable(Resources res, String urlString) {
            String key = extractKey(urlString);
            if (imageGetterReference.cache.containsKey(key)) {
                Drawable drawable = imageGetterReference.cache.getAsDrawable(key);
                scale = getScale(drawable);
                drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * scale), (int) (drawable.getIntrinsicHeight() * scale));
                return drawable;
            }
            try (InputStream is = fetch(urlString)) {
                Drawable drawable = new BitmapDrawable(res, is);
                scale = getScale(drawable);
                drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * scale), (int) (drawable.getIntrinsicHeight() * scale));
                imageGetterReference.cache.put(key, drawable);
                return drawable;
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
            }
            return null;
        }

        /**
         * Get the compressed image with specific quality from URL
         */
        public Drawable fetchCompressedDrawable(Resources res, String urlString) {
            Bitmap decoded = null;

            String key = extractKey(urlString);
            if (imageGetterReference.cache.containsKey(key)) {
                Drawable drawable = imageGetterReference.cache.getAsDrawable(key);
                scale = getScale(drawable);
                drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * scale), (int) (drawable.getIntrinsicHeight() * scale));
                return drawable;
            }

            try (InputStream is = fetch(urlString);
                     ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                Bitmap original = new BitmapDrawable(res, is).getBitmap();
                original.compress(Bitmap.CompressFormat.JPEG, qualityImage, out);
                original.recycle();
                decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
                return null;
            }
            scale = getScale(decoded);
            BitmapDrawable b = new BitmapDrawable(res, decoded);
            b.setBounds(0, 0, (int) (b.getIntrinsicWidth() * scale), (int) (b.getIntrinsicHeight() * scale));
            imageGetterReference.cache.put(key, b);
            return b;
        }

        private int calcWidth() {
            int screenWidth = 1024;
            Activity a = activity.get();
            if (a != null) {
                Display display = a.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                if (screenWidth >= size.x) {
                    screenWidth = (int) (size.x * 0.95f);
                }
            }
            return screenWidth;
        }

        private float getScale(Bitmap bitmap) {
            return ((float) calcWidth() / (float) bitmap.getWidth());
        }

        private float getScale(Drawable drawable) {
            return ((float)calcWidth() / (float)drawable.getIntrinsicWidth());
        }

        private InputStream fetch(String urlString) throws IOException {
            URL url;
            if (imageGetterReference == null) {
                return null;
            }
            url = URI.create(urlString).toURL();
            return (InputStream) url.getContent();
        }
    }

    @SuppressWarnings("deprecation")
    public static class UrlDrawable extends BitmapDrawable {
        protected Drawable drawable;

        @Override
        public void draw(Canvas canvas) {
            // override the draw to facilitate refresh function later
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }
    }
}
