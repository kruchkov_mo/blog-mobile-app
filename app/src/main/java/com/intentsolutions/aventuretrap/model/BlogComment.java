package com.intentsolutions.aventuretrap.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class BlogComment {
    public String text;
    public String dateAdded;
    public String userUid;
    public String userName;
    public String commentId;

    public BlogComment() {
    }

    public BlogComment(String commentId, String text, String dateAdded, String userUid, String userName) {
        this.text = text;
        this.dateAdded = dateAdded;
        this.userUid = userUid;
        this.commentId = commentId;
        this.userName = userName;
    }
}
