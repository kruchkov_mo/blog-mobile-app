package com.intentsolutions.aventuretrap.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class BlogUser {
    public String username;
    public String photo;
    public String uid;
    public String email;

    public BlogUser() {}

    public BlogUser(String uid, String username, String email, String photo) {
        this.uid = uid;
        this.username = username;
        this.email = email;
        this.photo = photo;
    }
}
