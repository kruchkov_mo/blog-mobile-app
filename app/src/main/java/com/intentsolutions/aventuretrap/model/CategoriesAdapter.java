package com.intentsolutions.aventuretrap.model;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.intentsolutions.aventuretrap.R;

import java.util.ArrayList;
import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CatViewHolder> {

    private List<Category> categories;
    private CategoryClickListener listener;
    private static final String TAG = "CategoriesAdapter";

    public class CatViewHolder extends RecyclerView.ViewHolder {
        public TextView name, description;

        public CatViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.cat_name);
            description = (TextView) view.findViewById(R.id.cat_desc);
        }
    }


    public CategoriesAdapter(List<Category> categories, CategoryClickListener listener) {
        if (categories == null) {
            this.categories = new ArrayList<>();
        } else {
            this.categories = categories;
        }
        this.listener = listener;
    }


    public interface CategoryClickListener {
        public void onItemClick(View v, int position);
    }


    @Override
    public CatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);

        final CatViewHolder holder = new CatViewHolder(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, holder.getPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(CatViewHolder holder, int position) {
        Category category = categories.get(position);
        holder.name.setText(category.getCategoryName());
        holder.description.setText(category.getCategoryDesk());

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}