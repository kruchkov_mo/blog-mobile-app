package com.intentsolutions.aventuretrap.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.intentsolutions.aventuretrap.cache.DbCache;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 2/5/2018.
 */

public class BannerCache extends DbCache<Banner> {
    private static final String TABLE_BANNER = "banner";

    private static final String KEY_BANNER_ID = "banner_id";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_URL = "url";
    private static final String KEY_VISIBILITY = "visibility";

    public BannerCache(Context context) {
        super(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_BANNER_TABLE = "CREATE TABLE " + TABLE_BANNER + "("
                + KEY_BANNER_ID + " INTEGER PRIMARY KEY,"
                + KEY_IMAGE + " TEXT,"
                + KEY_URL + " TEXT,"
                + KEY_VISIBILITY + " INTEGER DEFAULT 0)";
        db.execSQL(CREATE_BANNER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BANNER);
        onCreate(db);
    }

    @Override
    public List<Banner> getItems(final String SQL_QUERY) {
        List<Banner> banners = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SQL_QUERY, null);

        if (cursor.moveToFirst()) {
            do {
                Boolean isVisible = !cursor.getString(3).equals("0");
                Banner banner = new Banner();
                banner.setImg(cursor.getString(1));
                banner.setUrl(cursor.getString(2));
                banner.setVisible(isVisible);
                banners.add(banner);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return banners;
    }

    public List<Banner> getAllBanners() {
        return  getItems("SELECT * FROM " + TABLE_BANNER);
    }

    @Override
    public void add(Banner banner) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IMAGE, banner.getImg());
        values.put(KEY_URL, banner.getUrl());
        values.put(KEY_VISIBILITY, banner.getVisible() ? "1" : "0");

        db.insert(TABLE_BANNER, null, values);
        db.close();
    }

    public boolean cleanCategory() {
        return super.cleanTable(TABLE_BANNER);
    }
}