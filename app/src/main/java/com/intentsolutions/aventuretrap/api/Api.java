package com.intentsolutions.aventuretrap.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import com.intentsolutions.aventuretrap.model.Banner;
import com.intentsolutions.aventuretrap.model.Category;
import com.intentsolutions.aventuretrap.model.Post;

public interface Api {
    @GET("get-banner")
    Call<Banner> getBanner();

    @GET("get-posts")
    Call<List<Post>> getPosts(@Query("category_id") Integer categoryId);

    @GET("get-category")
    Call<List<Category>> getCategories();
}
