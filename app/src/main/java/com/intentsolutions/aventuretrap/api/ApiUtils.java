package com.intentsolutions.aventuretrap.api;

public class ApiUtils {
    //public static final String BASE_URL = "http://appv.apijornal.com/api/1/";

    public static Api getApiService(final String BASE_URL) {
        return RetrofitClient.getClient(BASE_URL).create(Api.class);
    }
}