package com.intentsolutions.aventuretrap.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.intentsolutions.aventuretrap.R;
import com.intentsolutions.aventuretrap.model.AuthUtils;
import com.intentsolutions.aventuretrap.model.BlogComment;
import com.intentsolutions.aventuretrap.model.BlogPost;
import com.intentsolutions.aventuretrap.model.CommentAdapter;
import com.intentsolutions.aventuretrap.model.CommentItem;
import com.intentsolutions.aventuretrap.model.IntentHttpImageGetter;
import com.intentsolutions.aventuretrap.model.Post;

import org.sufficientlysecure.htmltextview.HtmlResImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class PostActivity extends AppCompatActivity {
    Post post;
    @BindView(R.id.item_img)
    ImageView itemImg;
    /*@BindView(R.id.item_des)
    TextView itemDes;*/
    @BindView(R.id.html_text)
    HtmlTextView htmlText;
    Button btnAddComment;
    EditText editNewComment;

    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private FirebaseDatabase db;

    private void emptyPost() {
        Glide.with(this)
                .load("")
                .error(R.drawable.no_image)
                .into(itemImg);
        htmlText.setHtml(getString(R.string.no_post_data), new HtmlResImageGetter(htmlText));
        //itemDes.setText(getString(R.string.no_post_data));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        ButterKnife.bind(this);
        Typeface font = Typeface.createFromAsset(getAssets(), "Scada-Regular.ttf");
        if (savedInstanceState != null) {
            post = savedInstanceState.getParcelable("post");
        }
        Intent i = getIntent();
        if (i == null) {
            emptyPost();
            Log.d("Null pointer", "Intent instance is null");
            return;
        }
        Bundle b = i.getExtras();
        if (b == null) {
            emptyPost();
            Log.d("Null pointer", "Bundle instance is null");
            return;
        }
        post = b.getParcelable("post");

        if (post == null) {
            emptyPost();
            Log.d("Null pointer", "Bundle instance is null");
            return;
        }
        setTitle(post.getPostName());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        IntentHttpImageGetter imageGetter = new IntentHttpImageGetter(htmlText, this);
        htmlText.setHtml(post.getPostDesk(), imageGetter);
        htmlText.setTypeface(font);
        //itemDes.setText(Html.fromHtml(post.getPostDesk(), Html.FROM_HTML_MODE_COMPACT));
        if (post.getPostImage() != null) {
            Glide.with(this)
                    .load(post.getPostImage())
                    .error(R.drawable.no_image)
                    .into(itemImg);
        } else {
            itemImg.setVisibility(View.GONE);
        }

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        db = FirebaseDatabase.getInstance();

        updateUi(currentUser != null);
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        final String pid = post.getPostId();

        // add post data to db if not exists
        ValueEventListener responseListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    BlogPost blogPost = new BlogPost(pid, null, null);
                    AuthUtils.addNewPost(PostActivity.this.db.getReference("posts"), blogPost);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };
        db.getReference().child("posts").child(pid).addValueEventListener(responseListener);

        DatabaseReference ref = db.getReference().child("posts").child(pid);
        if (ref != null) {
            ref.addValueEventListener(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            BlogPost p = dataSnapshot.getValue(BlogPost.class);
                            updateLikes(p, pid);
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });
        }

        // comments
        // fetch comments
        db.getReference("posts").child(post.getPostId()).child("commentUserIds").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    GenericTypeIndicator<ArrayList<String>> objectsGTypeInd = new GenericTypeIndicator<ArrayList<String>>() {};
                    final List<String> commentsUsersIds = dataSnapshot.getValue(objectsGTypeInd);
                    if (commentsUsersIds != null) {
                        PostActivity.this.db.getReference("comments").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                List<CommentItem> result = new LinkedList<>();
                                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                    BlogComment bc = ds.getValue(BlogComment.class);
                                    if (bc != null && commentsUsersIds.contains(bc.commentId)) {
                                        result.add(new CommentItem(bc.userName, bc.dateAdded, bc.text));
                                    }
                                }
                                createCommentAdapter(result);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {}
                        });
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        // button for adding a new comment
        btnAddComment = findViewById(R.id.submit_comment);
        editNewComment = findViewById(R.id.new_comment_text);
        btnAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PostActivity.this.currentUser != null && PostActivity.this.db != null) {
                    DatabaseReference commentRef = PostActivity.this.db.getReference().child("comments");

                    final String newCommentId = commentRef.push().getKey();
                    Date d = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
                    String dateNow = sdf.format(d);
                    BlogComment newComment = new BlogComment(newCommentId,
                            editNewComment.getText().toString(),
                            dateNow,
                            PostActivity.this.currentUser.getUid(),
                            PostActivity.this.currentUser.getDisplayName());
                    AuthUtils.addNewComment(commentRef, newComment);
                    editNewComment.setText("");
                    final DatabaseReference postRef = PostActivity.this.db.getReference().child("posts").child(PostActivity.this.post.getPostId()).child("commentUserIds");
                    postRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            List<String> commentsUsersIds = null;
                            if (!dataSnapshot.exists()) {
                                commentsUsersIds = new LinkedList<>();
                                commentsUsersIds.add(newCommentId);
                            } else {
                                GenericTypeIndicator<ArrayList<String>> objectsGTypeInd = new GenericTypeIndicator<ArrayList<String>>() {};
                                commentsUsersIds = dataSnapshot.getValue(objectsGTypeInd);
                                if (commentsUsersIds != null) {
                                    if (!commentsUsersIds.contains(newCommentId)) {
                                        commentsUsersIds.add(newCommentId);
                                    }
                                }
                            }
                            postRef.setValue(commentsUsersIds);
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });
                }
            }
        });
    }

    private void createCommentAdapter(List<CommentItem> comments) {
        final RecyclerView commentList = findViewById(R.id.comment_list);
        CommentAdapter mAdapter = new CommentAdapter(comments);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        commentList.setLayoutManager(mLayoutManager);
        commentList.setItemAnimator(new DefaultItemAnimator());
        commentList.setAdapter(mAdapter);
    }

    private void updateUi(boolean isSignIn) {
        LinearLayout l = findViewById(R.id.add_comment);
        if (l != null) {
            l.setVisibility(isSignIn ? View.VISIBLE : View.GONE);
        }
    }

    private void updateLikes(BlogPost p, final String pid) {
        final List<String> list = p == null || p.likeUsersids == null ? new LinkedList<String>() : p.likeUsersids;
        if (list.size() > 0) {
            final TextView usersLike = findViewById(R.id.likes);
            usersLike.setText(String.format(getString(R.string.x_user_likes), list.size()));
            usersLike.setVisibility(View.VISIBLE);
        }
        if (currentUser != null && !list.contains(currentUser.getUid())) {
            final ImageView likeThis = findViewById(R.id.like_this);
            likeThis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(PostActivity.this, "You liked this post", Toast.LENGTH_LONG).show();
                    AuthUtils.likePost(db.getReference().child("posts").child(pid).child("likeUsersids").child(String.valueOf(list.size())), currentUser.getUid());
                    likeThis.setVisibility(View.GONE);
                }
            });
            likeThis.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("post", post);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}