package com.intentsolutions.aventuretrap.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.intentsolutions.aventuretrap.R;
import com.intentsolutions.aventuretrap.api.Api;
import com.intentsolutions.aventuretrap.api.ApiUtils;
import com.intentsolutions.aventuretrap.cache.CategoryCache;
import com.intentsolutions.aventuretrap.cache.PostCache;
import com.intentsolutions.aventuretrap.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;
    private RecyclerView postRecyclerView;
    private int currentCatId = 0;

    private Api mService;
    private static final int DEFAULT_AD_INTERVAL = 2;
    private SwipeRefreshLayout swipeRefresh;
    private PostAdapter adapter;
    private LinearLayout layoutError;
    private LinearLayout layoutCatError;
    private TextView loginItem;
    private TextView logoutItem;
    private FirebaseAuth auth;
    private FirebaseDatabase db;
    private SharedPreferences appSettings;
    private CardView cardView;
    private RelativeLayout bannerImg;
    private ImageView btnCloseBanner;

    private void addShortcut() {
        Intent shortcutIntent = new Intent(getApplicationContext(), MainActivity.class);

        shortcutIntent.setAction(Intent.ACTION_MAIN);

        Intent addIntent = new Intent();
        addIntent
                .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(getApplicationContext(),
                        R.mipmap.ic_launcher));

        addIntent
                .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        addIntent.putExtra("duplicate", false);  //may it's already there so don't duplicate
        getApplicationContext().sendBroadcast(addIntent);

        SharedPreferences.Editor prefEditor = appSettings.edit();
        prefEditor.putBoolean("shortcut", true);
        prefEditor.apply();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mService = ApiUtils.getApiService(getString(R.string.api_url));

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Info activity
        View infoView = findViewById(R.id.ellipse);
        infoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Swipe refresh posts
        swipeRefresh = this.findViewById(R.id.swipe_refresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MainActivity.this.getPostsByCatId(currentCatId);
            }
        });

        // add shortcut
        appSettings = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        if(!appSettings.getBoolean("shortcut", false)) {
            addShortcut();
        }

        // banner
        cardView = findViewById(R.id.card_view);
        bannerImg = findViewById(R.id.banner);
        btnCloseBanner = findViewById(R.id.button_cancel);
        btnCloseBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardView.setVisibility(View.GONE);
            }
        });

        // Categories
        recyclerView = findViewById(R.id.nav_drawer_recycler_view);
        if (recyclerView != null) {
            getCategories();
        }

        // Posts
        postRecyclerView = this.findViewById(R.id.post_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        postRecyclerView.setLayoutManager(layoutManager);

        Drawable drawableDivider = getDrawable(R.drawable.post_item_divider);
        if (drawableDivider != null) {
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(postRecyclerView.getContext(),
                    layoutManager.getOrientation());
            dividerItemDecoration.setDrawable(drawableDivider);
            postRecyclerView.addItemDecoration(dividerItemDecoration);
        }

        // Errors
        layoutError = findViewById(R.id.layout_post_error);
        layoutCatError = findViewById(R.id.layout_cat_error);

        // Auth
        loginItem = findViewById(R.id.nav_sign_in);
        loginItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        logoutItem = findViewById(R.id.nav_sign_out);
        logoutItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSignOutFunction();
            }
        });

        auth = FirebaseAuth.getInstance();
        Intent intent = getIntent();
        String result = intent.getStringExtra("auth_result");
        if (result != null) {
            Toast.makeText(MainActivity.this, "Login as " + result, Toast.LENGTH_LONG).show();
        }

        db = FirebaseDatabase.getInstance();
        Bundle data = intent.getExtras();

        final FirebaseUser currentUser = auth.getCurrentUser();
        updateUi(currentUser);
        if (data != null) {
            if (currentUser != null) {
                updateUi(currentUser);
                ValueEventListener responseListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.exists()) {
                            AuthUtils.addNewUser(db.getReference("users"), currentUser);
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                };
                db.getReference().child("users").child(currentUser.getUid()).addValueEventListener(responseListener);
            }
        }

        getBanner();
    }

    public void userSignOutFunction() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        auth.signOut();
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUi(null);
                        Toast.makeText(MainActivity.this, getString(R.string.you_are_sign_out), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateUi(final FirebaseUser currentUser) {
        boolean isSignIn = false;

        ImageView userAvatar;
        TextView userName;
        TextView email;

        if (currentUser == null) {
            email = findViewById(R.id.acc_email);
            email.setText("user@example.com");

            userName = findViewById(R.id.acc_login);
            userName.setText("user");

            userAvatar = findViewById(R.id.acc_avatar);
            userAvatar.setImageDrawable(getDrawable(R.mipmap.ic_launcher_round));
        } else {
            isSignIn = true;
            email = findViewById(R.id.acc_email);
            email.setText(currentUser.getEmail());

            userName = findViewById(R.id.acc_login);
            userName.setText(currentUser.getDisplayName());

            userAvatar = findViewById(R.id.acc_avatar);

            Glide.with(MainActivity.this)
                    .load(currentUser.getPhotoUrl())
                    .error(R.drawable.common_google_signin_btn_icon_disabled)
                    .into(userAvatar);
        }
        loginItem.setVisibility(isSignIn ? GONE : VISIBLE);
        logoutItem.setVisibility(isSignIn ? VISIBLE : GONE);
    }

    private void bannerError(final String text) {
        if (!text.startsWith("Unable to resolve")) {
            Toast.makeText(this,
                    getString(R.string.load_banner_error) + ": " + text,
                    Toast.LENGTH_SHORT).show();
            Log.e("banner", text);
        }
    }

    private void getBanner() {
        Call<Banner> getBanner = mService.getBanner();
        getBanner.enqueue(new Callback<Banner>() {
            @Override
            public void onResponse(Call<Banner> call, Response<Banner> response) {
                if (response.isSuccessful()) {
                    final Banner banner = response.body();
                    if (banner.getVisible()) {
                        SimpleTarget<Bitmap> target = new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
                                Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                                bannerImg.setBackground(drawable);
                                cardView.setVisibility(View.VISIBLE);
                            }
                        };
                        if (banner.getImg() == null) {
                            RequestManager requestManager = Glide.with(MainActivity.this);
                            DrawableTypeRequest<Integer> drawableTypeRequest =
                                    requestManager.load(R.drawable.banner_stub);
                            BitmapTypeRequest<Integer> bitmap = drawableTypeRequest.asBitmap();
                            bitmap.into(target);
                        } else {
                            Glide.with(MainActivity.this)
                                    .load(banner.getImg())
                                    .asBitmap()
                                    .into(target);
                            bannerImg.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(banner.getUrl()));
                                    startActivity(i);
                                    cardView.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                } else {
                    bannerError(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<Banner> call, Throwable t) {
                bannerError(t.getMessage());
            }
        });
    }

    private void categoryError(final String msg) {
        TextView textError = this.findViewById(R.id.text_cat_error);
        textError.setText(msg);
        layoutCatError.setVisibility(View.VISIBLE);
    }

    private void getCategories() {
        final CategoryCache categoryCache = new CategoryCache(this);
        mService.getCategories().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                final List<Category> categories;
                if (response.isSuccessful()) {
                    categories = response.body();
                    if (categories != null) {
                        // save to db
                        if (categories.size() > 0) {
                            categoryCache.cleanCategory();
                            categoryCache.save(categories);
                            createCategoriesAdapter(categories);
                            return;
                        }
                    }
                } else {
                    int statusCode  = response.code();
                    Log.d("dev", "Fetching category fails: " + statusCode);
                    categories = categoryCache.getAllCategories();
                    if (categories.size() < 1) {
                        Toast.makeText(MainActivity.this,
                                "Fetching category fails. Check your internet connection",
                                Toast.LENGTH_LONG).show();
                    } else {
                        createCategoriesAdapter(categories);
                        return;
                    }
                }
                categoryError("No categories found");
            }
            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                final List<Category> categories = categoryCache.getAllCategories();
                if (categories.size() > 0) {
                    createCategoriesAdapter(categories);
                    return;
                } else {
                    Toast.makeText(MainActivity.this,
                            "Fetching category fails. Check your internet connection",
                            Toast.LENGTH_LONG).show();
                    Log.d("dev", "Fetching category fails: " + t.getMessage());
                }
                categoryError("No categories found");
            }
        });
    }

    private List<Post> injectAd(List<Post> posts, final int interval) {
        Collections.sort(posts);
        if (interval < 2) {
            return posts;
        }
        List<Post> injectedPosts = new ArrayList<>();
        int i = 0;
        for (Post p : posts) {
            if ((i + 1) % interval == 0) {
                injectedPosts.add(null);
                injectedPosts.add(p);
                i += 2;
            } else {
                injectedPosts.add(p);
                ++i;
            }
        }
        if ((i + 1) % interval == 0) {
            injectedPosts.add(null);
        }
        return injectedPosts;
    }

    private boolean createAdapter(List<Post> posts) {
        if (posts == null) {
            return false;
        }
        int adInterval;
        try {
            String strInterval = MainActivity.this.getResources().getString(R.string.ad_interval);
            adInterval = Integer.parseInt(strInterval);
        } catch (NumberFormatException e) {
            Log.d("Parse error", e.getMessage());
            Log.d("Parse error", "Default interval " + DEFAULT_AD_INTERVAL + " is used");
            adInterval = DEFAULT_AD_INTERVAL;
        }
        adapter = new PostAdapter(this,
                injectAd(posts, adInterval),
                new PostAdapter.ClickListener() {
                    @Override public void onItemClick(int position, View v) {
                        Intent intent = new Intent(MainActivity.this, PostActivity.class);
                        intent.putExtra("post", adapter.getItem(position));
                        startActivity(intent);
                    }
                });
        postRecyclerView.setAdapter(adapter);
        swipeRefresh.setRefreshing(false);

        return true;
    }


    private List<Post> getPostsFromCache(final PostCache postCache, final int categoryId) {
        long postCount = postCache.getPostCount(categoryId);
        if (postCount > 0) {
            return postCache.getPostsByCat(categoryId);
        }
        displayError(getString(R.string.no_post_data));
        return new ArrayList<Post>();
    }

    private void getPostsByCatId(final int categoryId) {
        layoutError.setVisibility(GONE);
        final PostCache postCache = new PostCache(this);
        Call<List<Post>> callPosts = mService.getPosts(categoryId);
        callPosts.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                List<Post> posts = null;
                if (response.isSuccessful()) {
                    posts = response.body();
                    if (posts.size() < 1) {
                        posts = getPostsFromCache(postCache, categoryId);
                    }
                    for (Post p : posts) {
                        p.setCategoryId(categoryId);
                    }
                    // save to db
                    postCache.cleanPost(categoryId);
                    postCache.save(posts);

                } else {
                    posts = getPostsFromCache(postCache, categoryId);
                }
                createAdapter(posts);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.d("getPost", "onFailure: " + t.getMessage());
                List<Post> posts = null;
                long postCount = postCache.getPostCount(categoryId);
                if (postCount > 0) {
                    posts = postCache.getPostsByCat(categoryId);
                    Toast.makeText(MainActivity.this,
                            getString(R.string.cache_data_is_used),
                            Toast.LENGTH_SHORT).show();
                    if (!createAdapter(posts)) {
                        displayError(getString(R.string.no_post_data));
                    }
                } else {
                    displayError(getString(R.string.need_inet));
                    swipeRefresh.setRefreshing(false);
                }
            }
        });
    }

    protected void displayError(final String message) {
        Toast.makeText(MainActivity.this, message,
                Toast.LENGTH_SHORT).show();
        TextView textError = this.findViewById(R.id.text_post_error);
        textError.setText(message);
        layoutError.setVisibility(View.VISIBLE);
    }

    private void createCategoriesAdapter(final List<Category> categories) {
        CategoriesAdapter mAdapter = new CategoriesAdapter(categories, new CategoriesAdapter.CategoryClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                currentCatId = categories.get(position).getCategoryId();
                getPostsByCatId(currentCatId);
            }
        });

            // open first category
        if (categories.size() > 0) {
            currentCatId = categories.get(0).getCategoryId();
            getPostsByCatId(currentCatId);
        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        /*int id = item.getItemId();

        if (id == R.id.nav_share) {
            // Handle the camera action
        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
